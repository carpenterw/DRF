# 自定义序列化器类
# 文件名和自定义类名根据需求随意定义
# 继承DRF的序列化器类
from rest_framework import serializers

from books.models import BookInfo


class BookSerializer(serializers.Serializer):
    # 指定序列化返回的字段，根据模型类指定，也可根据业务需求指定
    # 序列化器的字段类型和模型类的字段类型是一样的
    # id 不需要参与反序列化的验证保存，只需要进行结果返回
    # read_only 参数规定了该字段只参与序列化返回过程
    id = serializers.IntegerField(read_only=True)
    # 序列化长度验证[4,10]
    btitle = serializers.CharField(min_length=4, max_length=10)
    # write_only 参数规定了该字段只参与反序列化返回过程
    bpub_date = serializers.DateField(write_only=True)
    # 验证数值大小, 默认值
    bread = serializers.IntegerField(min_value=0, max_value=100, default=0)
    bcomment = serializers.IntegerField(default=0)

    # 表示前端嵌套返回英雄信息的字段，图书是主表，英雄是副表
    # 返回数据有三种形式

    # 1. 返回对应的id值：primaryKeyRelateField会将关联的英雄id返回
    # 一本图书对应多个英雄信息，要增加一个参数many，设置为true
    # heroinfo_set = serializers.PrimaryKeyRelatedField(read_only=True, many=True)

    # 2. StringRelateField返回关联表模型类中的__str__方法的数据内容
    # heroinfo_set = serializers.StringRelatedField(many=True)

    # 3. 单独定义关联表序列化器，根据定义的序列化器字段返回
    # heroinfo_set = HeroSerializer(many=True)


    # 单一字段验证方法，书写形式validate_字段名
    def validate_btitle(self, attrs):
        """
        验证btitle字段方法
        :param attrs: 接收要验证字段的数据
        :return:
        """
        # 验证图书的名字是否是python，是python则报错
        if attrs == 'python':
            raise serializers.ValidationError('书名不能为Python')

        # 注意：验证没问题，将验证后的数据返回
        return attrs


    # 多个字段验证
    def validate(self, attrs):
        """

        :param attrs: 接收的是所有验证的字段数据
        :return:
        """
        # 验证阅读量不能小于评论量
        if attrs['bread'] < attrs['bcomment']:
            raise serializers.ValidationError('阅读量不能小于评论量')

        return attrs
    # 封装保存业务逻辑
    def create(self, validated_data):
        """
        :param validated_data: 验证后的数据
        :return: 保存的数据对象
        """
        book = BookInfo.objects.create(**validated_data)
        # 保存完成后需要将保存后的数据对象返回
        return book

    # 封装更新业务逻辑
    def update(self, instance, validated_data):
        """
        :param instance: 要更新的数据对象(更新前的对象)
        :param validated_data: 验证后的字段数据
        :return:
        """
        # 直接字段更新
        # instance.btitle = validated_data['btitle']
        # instance.save()
        # 使用模型类的方法进行更新数据
        res = BookInfo.objects.filter(id=instance.id).update(**validated_data)
        book = BookInfo.objects.get(id=instance.id)

        return book


class HeroSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    hname = serializers.CharField()
    hgender = serializers.IntegerField()
    hcomment = serializers.CharField()

    # 主副表嵌套，不能同时相互嵌套，一次只能嵌套一个
    # 副表嵌套主表，使用外键字段
    # 1. 主键
    # hbook = serializers.PrimaryKeyRelatedField(read_only=True)

    # 2. __str__ 方法对应的字段
    # hbook = serializers.StringRelatedField()

    # 3.一个英雄对应一本书
    hbook = BookSerializer()


class BookModelSerializer(serializers.ModelSerializer):
    """
    模型类序列化器
    和Serializer区别
    1. 可以根据指定的模型类自动生成序列化器字段
    2. 帮助实现了create和update业务逻辑
    3. 帮助实现唯一值判断方法，只要在模型类中对字段指定了字段的唯一值选项参数（unique=true）则会自动进行判断
    除了这三点，其他使用形式都一样
    """
    # 显示指定的字段
    # 修改字段的选项，方法2
    bcomment = serializers.IntegerField(min_value=0, max_value=100, default=0)

    # 定义模型类没有的字段，不参与保存数据库，只参与数据的获取和验证
    # 可以额外定义新的字段
    # sms_code = serializers.CharField(min_length=6, max_length=6)
    class Meta:
        # 指定根据哪个模型类生成对应的序列化器字段
        model = BookInfo
        # 指定字段内容， __all__ 表示将所有模型类字段生成序列化器字段
        fields = "__all__"
        # 指定单独字段，元组只有一个元素，要加逗号
        # fields = ('btitle', 'bread')
        # 可以使用列表指定字段
        # fields = ['btitle']
        # 字段取反, 不包含这里的字段
        # exclude = ('btitle', 'bread')

        # 修改字段的选项内容,方法1
        extra_kwargs = {
            # key 值为要修改的字段，value值为修改的选项内容
            'bread': {
                'max_value': 100,
                'min_value': 0,
                'default': 0
            }
        }