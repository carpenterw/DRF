# 拓展类子类在generics下面
from rest_framework.generics import GenericAPIView, CreateAPIView, ListAPIView, ListCreateAPIView, RetrieveAPIView, \
    UpdateAPIView, DestroyAPIView, RetrieveDestroyAPIView, RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from books.serializers import BookModelSerializer
from books.models import BookInfo
# 导入匿名用户限流类
from rest_framework.throttling import AnonRateThrottle

class BooksView(ListCreateAPIView):

    # 指定查询集属性
    queryset = BookInfo.objects.all()
    # 指定序列化器属性
    serializer_class = BookModelSerializer

    # 按照视图进行限流
    throttle_scope = 'listcreateapiview'


class BookView(RetrieveUpdateDestroyAPIView):

    # 指定查询集属性
    queryset = BookInfo.objects.all()
    # 指定序列化器属性
    serializer_class = BookModelSerializer

    # 局部配置限流
    throttle_classes = [AnonRateThrottle]



