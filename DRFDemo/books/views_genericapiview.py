# from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from books.serializers import BookModelSerializer
from books.models import BookInfo

class BooksView(GenericAPIView):
    """
    保存图书
    """

    # 指定查询集属性
    queryset = BookInfo.objects.all()
    # 指定序列化器属性
    serializer_class = BookModelSerializer
    def post(self, request):
        """
        保存图书信息
        :param request:
        :return:
        """
        # 接收前端传递的数据
        data = request.data

        # 初始化序列化器对象，传入验证参数
        # get_serializer会获取serializer_class指定的序列化器
        ser = self.get_serializer(data=data)

        # 2. 验证方法2
        # is_valid 序列化器提供的验证方法， raise_exception=True的作用
        # 是验证失败抛出异常
        try:
            ser.is_valid(raise_exception=True)
        except Exception as e:
            print(e)
            return Response({'error': '数据验证失败'})
        # 保存数据

        # 调用封装的保存业务逻辑
        ser.save()

        # 返回结果
        return Response(ser.data)


    def get(self, request):
        """
        获取所有图书信息
        :param request:
        :return:
        """
        # 获取所有图书
        # get_queryset 获取queryset属性指定的所有数据
        books = self.get_queryset()

        # 使用序列化器返回多个数据
        # 返回多个数据，初始化时增加many参数，为true
        ser = self.get_serializer(books, many=True)
        return Response({'books': ser.data})


class BookView(GenericAPIView):
    """
    删除一本图书信息
    """

    # 指定查询集属性
    queryset = BookInfo.objects.all()
    # 指定序列化器属性
    serializer_class = BookModelSerializer

    def delete(self, request, pk):
        """
        删除图书
        :param request:
        :param id:
        :return:
        """
        # 根据id查询图书
        try:
            # get_object 获取queryset属性中的一个数据对象，会根据传递的id值自动判断
            book = self.get_object()
        except:
            return Response({'code': 400, 'errmsg': '图书不存在'})

        # 逻辑删除
        book.is_delete = True
        book.save()

        # 返回结果
        return Response({})

    def put(self, request, pk):
        """
        更新一本图书
        :param request:
        :return:
        """
        # 根据id获取图书
        try:
            # 根据传递的id自动判断
            book = self.get_object()
        except:
            return Response({'code': 400, 'errmsg': '数据不存在'})

        # 获取更新的字段
        data = request.data
        # 参数一个是数据对象，一个是传递数据
        ser = self.get_serializer(book, data=data)
        try:
            ser.is_valid(raise_exception=True)
        except:
            return Response({'error': '字段验证失败'})
        # 更新图书信息
        # 调用序列化封装的update更新方法
        # save()方法通过序列化器初始化是否传递数据对象来确定调用create方法还是update方法
        ser.save()


        # 返回结果
        return Response(ser.data)

    def get(self, request, pk):
        """
        获取一本图书
        :param request:
        :return:
        """
        # 获取单一图书
        try:
            book = self.get_object()
        except:
            return Response({'code': 400, 'errmsg': '数据不存在'})

        ser = self.get_serializer(book)

        # 返回结果
        return Response(ser.data)