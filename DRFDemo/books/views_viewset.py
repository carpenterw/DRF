# 导入视图集
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from books.models import BookInfo
from books.serializers import BookModelSerializer
from rest_framework.response import Response
# 导入权限类方法
from rest_framework.permissions import IsAuthenticated
# 导入认证类方法
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
# 导入限流类方法
from rest_framework.throttling import UserRateThrottle

# 导入排序类方法
from rest_framework.filters import OrderingFilter

# 导入自定义的分页器
from books.utils import NumPage, NumPageTwo

class BooksView(ModelViewSet):
    # 指定查询集属性
    queryset = BookInfo.objects.all()
    # 指定序列化器属性
    serializer_class = BookModelSerializer

    # 局部认证和局部权限
    # 权限
    permission_classes = [IsAuthenticated]
    # 认证
    authentication_classes = [BasicAuthentication, SessionAuthentication]

    # 限流属性局部配置
    # throttle_classes = [UserRateThrottle]

    # 按照视图类进行限流， 指定限流名称
    throttle_scope = 'modelviewset'

    # 指定结果返回时的过滤字段
    filter_fields = ('btitle', 'bread')

    # 指定排序方法
    filter_backends = [OrderingFilter]

    # 指定排序字段
    ordering_fields = ['id', 'bread']

    # 指定自定义分页器
    # pagination_class = NumPage

    pagination_class = NumPageTwo

    # 自定义获取最后一本图书的方法
    # methods 指定该方法匹配的请求方式
    # detail 指定需不需要匹配id值，false 是不需要
    @action(methods=['get'], detail=False)
    def last_book(self, request):
        book = BookInfo.objects.last()
        ser = self.serializer_class(book)
        return Response(ser.data)