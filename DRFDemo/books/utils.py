# DRF 封装的分页器
from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination

# 自定义分页器
class NumPage(PageNumberPagination):
    # 指定分页的相关属性
    # 指定页容量属性---每页展示的数量
    max_page_size = 5

    # 指定页容量的请求参数， 前端请求时可以通过 page_size 指定需要的数量，但是不能超过最大页容量
    page_size_query_param = 'page_size'

class NumPageTwo(LimitOffsetPagination):
    max_limit = 5
    # limit_query_param = 'limit'
