from django.shortcuts import render

# Create your views here.
from django.views import View
from django import http
import json

from books.models import BookInfo, HeroInfo
# 导入自定义的序列化器
from books.serializers import BookSerializer, HeroSerializer

class BooksView(View):
    """
    保存图书
    """
    def post(self, request):
        # 接收前端传递的数据
        json_data = request.body.decode()
        dict_data = json.loads(json_data)

        # btitle = dict_data.get('btitle')
        # bpub_date = dict_data.get('bpub_date')
        # bread = dict_data.get('bread')
        # 验证数据
        # if not all([btitle, bpub_date]):
        #     return http.JsonResponse({'code': 400, 'errmsg': '缺少参数'})

        # 初始化序列化器对象，传入验证参数
        ser = BookSerializer(data=dict_data)
        # 调用验证方法
        # ser.is_valid()
        # 1. 验证方法1
        # print(ser.errors)
        # # 在保存数据之前需要对验证内容进行判断
        # if ser.errors:
        #     # ser.errors 有数据，说明验证失败，需要进行返回
        #     return http.JsonResponse({'error': ser.errors})

        # 2. 验证方法2
        # is_valid 序列化器提供的验证方法， raise_exception=True的作用
        # 是验证失败抛出异常
        try:
            ser.is_valid(raise_exception=True)
        except Exception as e:
            print(e)
            return http.JsonResponse({'error': '数据验证失败'})
        # 保存数据
        # 获取验证成功的字段数据进行保存， validated_data
        data = ser.validated_data
        # book = BookInfo.objects.create(**data)

        # 调用封装的保存业务逻辑
        ser.save()

        # book = BookInfo.objects.create(btitle=btitle, bpub_date=bpub_date)

        # 返回结果

        # 使用序列化器
        # 初始化生成序列化器对象
        # ser = BookSerializer(book)
        # 通过构造好的序列化器对象的data方法返回结果
        return http.JsonResponse(ser.data)

        # return http.JsonResponse({
        #     'id': book.id,
        #     'btitle': book.btitle,
        #     'bpub_date': book.bpub_date,
        #     'bread': book.bread
        # })

    def get(self, request):
        """
        获取所有图书信息
        :param request:
        :return:
        """
        # 获取所有图书
        books = BookInfo.objects.all()
        # book_list = []
        #
        # for book in books:
        #     book_list.append({
        #         'id': book.id,
        #         'btitle': book.btitle,
        #         'bpub_date': book.bpub_date,
        #         'bread': book.bread
        #     })

        # 使用序列化器返回多个数据
        # 返回多个数据，初始化时增加many参数，为true
        ser = BookSerializer(books, many=True)
        return http.JsonResponse({'books': ser.data})

        # return http.JsonResponse({'book_list': book_list})


class BookView(View):
    """
    删除一本图书信息
    """
    def delete(self, request, id):
        """
        删除图书
        :param request:
        :param id:
        :return:
        """
        # 根据id查询图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '图书不存在'})

        # 逻辑删除
        book.is_delete = True
        book.save()

        # 返回结果
        return http.JsonResponse({})

    def put(self, request, id):
        """
        更新一本图书
        :param request:
        :return:
        """
        # 根据id获取图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '数据不存在'})

        # 获取更新的字段
        json_data = request.body.decode()
        dict_data = json.loads(json_data)
        # 参数一个是数据对象，一个是传递数据
        ser = BookSerializer(book, data=dict_data)
        try:
            ser.is_valid(raise_exception=True)
        except:
            return http.JsonResponse({'error': '字段验证失败'})
        # 更新图书信息
        # 调用序列化封装的update更新方法
        # save()方法通过序列化器初始化是否传递数据对象来确定调用create方法还是update方法
        ser.save()


        # 返回结果
        return http.JsonResponse(ser.data)

    def get(self, request, id):
        """
        获取一本图书
        :param request:
        :return:
        """
        # 获取单一图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '数据不存在'})

        # 关联查询，返回图书时将对应的英雄信息一块返回，主表查询副表，一对多关系
        # heros = book.heroinfo_set.all()
        # heros = book.heroinfo_set.filter(hname__contains='段')
        # 提取英雄字段数据
        # hero_list = []
        # for hero in heros:
        #     hero_list.append({
        #         'id': hero.id,
        #         'hname': hero.hname,
        #         'hcomment': hero.hcomment
        #     })

        # 返回结果
        return http.JsonResponse({
            'id': book.id,
            'btitle': book.btitle,
            'bpub_date': book.bpub_date,
            # 'heros': hero_list
            'bread': book.bread
        })


class HerosView(View):
    def get(self, request):
        """
        获取所有英雄信息
        :param request:
        :return:
        """
        # 获取所有英雄
        heros = HeroInfo.objects.all()
        # book_list = []
        #
        # for book in books:
        #     book_list.append({
        #         'id': book.id,
        #         'btitle': book.btitle,
        #         'bpub_date': book.bpub_date,
        #         'bread': book.bread
        #     })

        # 使用序列化器返回多个数据
        # 返回多个数据，初始化时增加many参数，为true
        # ser = BookSerializer(books, many=True)
        # return http.JsonResponse({'books': ser.data})

        ser = HeroSerializer(heros, many=True)
        return http.JsonResponse({'heros': ser.data})
        # return http.JsonResponse({'book_list': book_list})

class HeroView(View):
    def get(self, request, id):
        """
        获取一个英雄
        :param request:
        :return:
        """
        # 获取一个英雄
        try:
            # book = BookInfo.objects.get(id=id)
            hero = HeroInfo.objects.get(id=id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '数据不存在'})

        # 关联查询，返回图书时将对应的英雄信息一块返回，主表查询副表，一对多关系
        # heros = book.heroinfo_set.all()
        # heros = book.heroinfo_set.filter(hname__contains='段')
        # 提取英雄字段数据
        # hero_list = []
        # for hero in heros:
        #     hero_list.append({
        #         'id': hero.id,
        #         'hname': hero.hname,
        #         'hcomment': hero.hcomment
        #     })

        # 返回结果
        ser = HeroSerializer(hero)
        return http.JsonResponse(ser.data)
        # return http.JsonResponse({
        #     'id': book.id,
        #     'btitle': book.btitle,
        #     'bpub_date': book.bpub_date,
        #     # 'heros': hero_list
        #     'bread': book.bread
        # })

