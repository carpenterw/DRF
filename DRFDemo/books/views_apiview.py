from rest_framework.views import APIView
from rest_framework.response import Response
from books.serializers import BookModelSerializer
from books.models import BookInfo

class BooksView(APIView):
    """
    保存图书
    """
    def post(self, request):
        # 接收前端传递的数据
        data = request.data

        # 初始化序列化器对象，传入验证参数
        ser = BookModelSerializer(data=data)

        # 2. 验证方法2
        # is_valid 序列化器提供的验证方法， raise_exception=True的作用
        # 是验证失败抛出异常
        try:
            ser.is_valid(raise_exception=True)
        except Exception as e:
            print(e)
            return Response({'error': '数据验证失败'})
        # 保存数据

        # 调用封装的保存业务逻辑
        ser.save()

        # 返回结果
        return Response(ser.data)


    def get(self, request):
        """
        获取所有图书信息
        :param request:
        :return:
        """
        # 获取所有图书
        books = BookInfo.objects.all()

        # 使用序列化器返回多个数据
        # 返回多个数据，初始化时增加many参数，为true
        ser = BookModelSerializer(books, many=True)
        return Response({'books': ser.data})


class BookView(APIView):
    """
    删除一本图书信息
    """
    def delete(self, request, id):
        """
        删除图书
        :param request:
        :param id:
        :return:
        """
        # 根据id查询图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return Response({'code': 400, 'errmsg': '图书不存在'})

        # 逻辑删除
        book.is_delete = True
        book.save()

        # 返回结果
        return Response({})

    def put(self, request, id):
        """
        更新一本图书
        :param request:
        :return:
        """
        # 根据id获取图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return Response({'code': 400, 'errmsg': '数据不存在'})

        # 获取更新的字段
        data = request.data
        # 参数一个是数据对象，一个是传递数据
        ser = BookModelSerializer(book, data=data)
        try:
            ser.is_valid(raise_exception=True)
        except:
            return Response({'error': '字段验证失败'})
        # 更新图书信息
        # 调用序列化封装的update更新方法
        # save()方法通过序列化器初始化是否传递数据对象来确定调用create方法还是update方法
        ser.save()


        # 返回结果
        return Response(ser.data)

    def get(self, request, id):
        """
        获取一本图书
        :param request:
        :return:
        """
        # 获取单一图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return Response({'code': 400, 'errmsg': '数据不存在'})

        ser = BookModelSerializer(book)

        # 返回结果
        return Response(ser.data)