from django.shortcuts import render

# Create your views here.
from django.views import View
from django import http
import json

from books.models import BookInfo

class BooksView(View):
    def post(self, request):
        # 接收前端传递的数据
        json_data = request.body.decode()
        dict_data = json.loads(json_data)

        btitle = dict_data.get('btitle')
        bpub_date = dict_data.get('bpub_date')
        bread = dict_data.get('bread')
        # 验证数据
        if not all([btitle, bpub_date]):
            return http.JsonResponse({'code': 400, 'errmsg': '缺少参数'})

        # 保存数据
        book = BookInfo.objects.create(btitle=btitle, bpub_date=bpub_date)

        # 返回结果
        return http.JsonResponse({
            'id': book.id,
            'btitle': book.btitle,
            'bpub_date': book.bpub_date,
            'bread': book.bread
        })

    def get(self, request):
        """
        获取所有图书信息
        :param request:
        :return:
        """
        # 获取所有图书
        books = BookInfo.objects.all()
        book_list = []

        for book in books:
            book_list.append({
                'id': book.id,
                'btitle': book.btitle,
                'bpub_date': book.bpub_date,
                'bread': book.bread
            })

        return http.JsonResponse({'book_list': book_list})




class BookView(View):
    """
    删除一本图书信息
    """
    def delete(self, request, id):
        """
        删除图书
        :param request:
        :param id:
        :return:
        """
        # 根据id查询图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '图书不存在'})

        # 逻辑删除
        book.is_delete = True
        book.save()

        # 返回结果
        return http.JsonResponse({})

    def put(self, request, id):
        """
        更新一本图书
        :param request:
        :return:
        """
        # 根据id获取图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '数据不存在'})

        # 获取更新的字段
        json_data = request.body.decode()
        dict_data = json.loads(json_data)

        btitle = dict_data.get('btitle')
        bpub_date = dict_data.get('bpub_date')
        bread = dict_data.get('bread')
        # 更新图书信息
        BookInfo.objects.filter(id=id).update(**dict_data)
        book = BookInfo.objects.get(id=id)

        # 返回结果
        return http.JsonResponse({
            'id': book.id,
            'btitle': book.btitle,
            'bpub_date': book.bpub_date
        })

    def get(self, request, id):
        """
        获取一本图书
        :param request:
        :return:
        """
        # 获取单一图书
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return http.JsonResponse({'code': 400, 'errmsg': '数据不存在'})

        # 关联查询，返回图书时将对应的英雄信息一块返回，主表查询副表，一对多关系
        # heros = book.heroinfo_set.all()
        # heros = book.heroinfo_set.filter(hname__contains='段')
        # 提取英雄字段数据
        # hero_list = []
        # for hero in heros:
        #     hero_list.append({
        #         'id': hero.id,
        #         'hname': hero.hname,
        #         'hcomment': hero.hcomment
        #     })

        # 返回结果
        return http.JsonResponse({
            'id': book.id,
            'btitle': book.btitle,
            'bpub_date': book.bpub_date,
            # 'heros': hero_list
            'bread': book.bread
        })

