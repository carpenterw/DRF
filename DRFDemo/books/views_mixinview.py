# from rest_framework.views import APIView
# 拓展类必须配合 GenericAPIView 一块使用
from rest_framework.mixins import ListModelMixin, CreateModelMixin, \
    UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from books.serializers import BookModelSerializer
from books.models import BookInfo

class BooksView(GenericAPIView, ListModelMixin, CreateModelMixin):
    """
    保存图书
    """

    # 指定查询集属性
    queryset = BookInfo.objects.all()
    # 指定序列化器属性
    serializer_class = BookModelSerializer
    def post(self, request):
        """
        保存图书信息
        :param request:
        :return:
        """
        return self.create(request)



    def get(self, request):
        """
        获取所有图书信息
        :param request:
        :return:
        """
        # 获取所有图书
        return self.list(request)


class BookView(GenericAPIView, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin):
    """
    删除一本图书信息
    """

    # 指定查询集属性
    queryset = BookInfo.objects.all()
    # 指定序列化器属性
    serializer_class = BookModelSerializer

    def delete(self, request, pk):
        """
        删除图书
        :param request:
        :param id:
        :return:
        """
        return self.destroy(request)

    def put(self, request, pk):
        """
        更新一本图书
        :param request:
        :return:
        """
        return self.update(request)

    def get(self, request, pk):
        """
        获取一本图书
        :param request:
        :return:
        """
        # 获取单一图书
        return self.retrieve(request)