from django.urls import path, include
from . import views, views_serializer, views_apiview, \
    views_genericapiview, views_mixinview, views_childview, \
    views_viewset

# DRF 提供的视图集自动生成路由类
from rest_framework.routers import SimpleRouter, DefaultRouter
# 初始化生成路由类对象
# 没有匹配首页路由（根路径的匹配）
# router = SimpleRouter()

# 匹配首页路由
router = DefaultRouter()
# 路由的注册生成
# 第一个参数： 路由前缀
# 第二个参数： 调用的视图类
# 第三个参数： 路由命名
# router.register(路由前缀, 调用的视图类, 路由命名)
router.register('viewsetbooks', views_viewset.BooksView, basename='books')
# 获取路由结果信息, 需要将生成的路由结果添加到urlpatterns路由列表中
# print(router.urls)

urlpatterns = [
    # 保存图书信息
    # path('books/', views.BooksView.as_view()),
    # 删除图书信息
    # <int:id>中间不要空格
    # path('books/<int:id>/', views.BookView.as_view()),


# 保存图书信息
    path('books/', views_serializer.BooksView.as_view()),
    # 删除图书信息
    # <int:id>中间不要空格
    path('books/<int:id>/', views_serializer.BookView.as_view()),

    # 获取所有英雄信息
    path('heros/', views_serializer.HerosView.as_view()),
    # 获取一个英雄
    path('heros/<int:id>/', views_serializer.HeroView.as_view()),

    # <-----APIView的使用----->
    path('apibooks/', views_apiview.BooksView.as_view()),
    # 删除图书信息
    # <int:id>中间不要空格
    path('apibooks/<int:id>/', views_apiview.BookView.as_view()),

    # <-----genericAPIView的使用----->
    path('gapibooks/', views_genericapiview.BooksView.as_view()),
    # 删除图书信息
    # <int:id>中间不要空格
    # 如果使用视图中的get_object方法必须匹配值为pk
    path('gapibooks/<int:pk>/', views_genericapiview.BookView.as_view()),

    # <-----拓展类的使用----->
    path('mixinbooks/', views_mixinview.BooksView.as_view()),
    # 删除图书信息
    # <int:id>中间不要空格
    # 如果使用视图中的get_object方法必须匹配值为pk
    path('mixinbooks/<int:pk>/', views_mixinview.BookView.as_view()),


    # <-----拓展类子类的使用----->
    path('childbooks/', views_childview.BooksView.as_view()),
    # 删除图书信息
    # <int:id>中间不要空格
    # 如果使用视图中的get_object方法必须匹配值为pk
    path('childbooks/<int:pk>/', views_childview.BookView.as_view()),


    # <-----视图集的使用----->
    # 视图集中可以通过字典的键值对形式将请求方式对应的方法绑定在一起
    # path('viewsetbooks/', views_viewset.BooksView.as_view({'post':'create', 'get':'list'})),

    # path('viewsetbooks/<int:pk>/', views_viewset.BooksView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    # 自定义方法，把方法名放到路径当中
    # path('viewsetbooks/last_book/', views_viewset.BooksView.as_view({'get':'last_book'})),

    # 第1种方式
    # path('', include(router.urls))
]
# 第2种方式
# urlpatterns.append(router.urls)
# 第3种方式， 生成路由列表添加到路由列表中
urlpatterns += router.urls
